package cvut.fel.sin.service;

import cvut.fel.sin.dto.request.BookCreate;
import cvut.fel.sin.exception.FieldInvalidException;
import cvut.fel.sin.entity.Author;
import cvut.fel.sin.entity.Publisher;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest()
public class PublisherServiceTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private PublisherService service;

    @Autowired
    private BookService bookService;

    @BeforeEach
    public void setUp(){
    }

    @Test
    public void createContract_newContract() {
        Author author = generateAuthor();
        Publisher publisher = generatePublisher();

        boolean result = service.createContract(author.getId(), publisher.getId());

        assertTrue(result);
    }

    @Test(expected = FieldInvalidException.class)
    public void createContract_assignedAuthor_exceptionExpected() {
        Author author = generateAuthor();
        Publisher publisher = generatePublisher();

        service.createContract(author.getId(), publisher.getId());
        service.createContract(author.getId(), publisher.getId());
    }

    @Test
    public void publishNewBook_newBook(){
        BookCreate request = generateBookCreationRequest();
        Publisher publisher = generatePublisher();

        long result = service.publishNewBook(publisher.getId(), request);

        assertEquals(4, result);
    }

    @Test(expected = FieldInvalidException.class)
    public void publishNewBook_publishedBook_expectException(){
        BookCreate request = generateBookCreationRequest();
        Publisher publisher = generatePublisher();

        service.publishNewBook(publisher.getId(), request);
        System.out.println("Hello");
        publisher.getBooks().forEach(System.out::println);
        service.publishNewBook(publisher.getId(), request);

    }


    private Author generateAuthor(){
        Author author = new Author("Kafka", "kafka@seznam.cz");
        em.persist(author);
        return author;
    }

    private Publisher generatePublisher(){
        Publisher publisher = new Publisher("UK");
        em.persist(publisher);
        return publisher;
    }

    private BookCreate generateBookCreationRequest(){
        List<Long> authorList = new ArrayList<>();
        authorList.add(generateAuthor().getId());

        BookCreate.BookCreateBuilder builder =  BookCreate.builder();
        builder.name("Drakula");
        builder.isbn("123");
        builder.genre(1L);
        builder.authors(authorList);
        return builder.build();
    }
}
