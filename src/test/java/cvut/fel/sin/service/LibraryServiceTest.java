package cvut.fel.sin.service;

import cvut.fel.sin.exception.FieldInvalidException;
import cvut.fel.sin.entity.Book;
import cvut.fel.sin.entity.Library;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest()
public class LibraryServiceTest {

    @PersistenceContext
    private EntityManager em;


    @Autowired
    private LibraryService service;

    @BeforeEach
    public void setUp(){
    }

    @Test
    public void addBookToLibrary_newBook(){
        Book book = generateBook();
        Library library = generateLibrary();

        boolean result = service.addBookToLibrary(library.getId(), book.getId());

        assertTrue(result);
    }

    @Test(expected =  FieldInvalidException.class)
    public void addBookToLibrary_existingBook_expectException(){
        Book book = generateBook();
        Library library = generateLibrary();

        service.addBookToLibrary(library.getId(), book.getId());

        service.addBookToLibrary(library.getId(), book.getId());
    }

    private Book generateBook(){
        Book book = new Book("name");
        em.persist(book);
        return book;
    }

    private Library generateLibrary(){
        Library library = new Library();
        library.setName("My_new_library");
        em.persist(library);
        return library;
    }
}
