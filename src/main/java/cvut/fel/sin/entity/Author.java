package cvut.fel.sin.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "author")
@Getter @Setter
@NoArgsConstructor
public class Author extends AbstractEntity{

//    @NotNull
    private String surname;
    private String email;

    @ManyToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JoinTable(
            name = "book_author",
            joinColumns = @JoinColumn(name = "book_id",
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "author_id",
                    referencedColumnName = "id")
    )
    private List<Book> books;

    @ManyToMany(mappedBy = "authors")
    private List<Publisher> publishers;

    @OneToOne
    @JoinTable(name = "address_id")
    private Address address;

    public Author(String name, String surname) {
        this();
        this.name = name;
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
