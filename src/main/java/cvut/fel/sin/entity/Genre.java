package cvut.fel.sin.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

public enum Genre {
    FICTION,
    MYSTERY,
    SCIENCE_FICTION,
    FANTASY,
    ROMANCE,
    HORROR,
    THRILLER,
    NON_FICTION,
    BIOGRAPHY,
    SELF_HELP,
    HISTORY,
    POETRY,
    DRAMA,
    COMEDY,
    CRIME,
    ADVENTURE,
    SCIENCE,
    PHILOSOPHY,
    RELIGION,
    COOKING,
    ART,
    TRAVEL,
    CHILDRENS,
    OTHER
}
