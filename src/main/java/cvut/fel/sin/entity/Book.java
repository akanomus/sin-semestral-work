package cvut.fel.sin.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "book")
@Getter @Setter
public class Book extends AbstractEntity {

    @Column(unique = true)
    private String ISBN;
    private Date published;

    @ManyToMany(mappedBy = "books")
    private List<Library> libraries;

    @ManyToOne
    @JoinColumn(
            name = "publisher_id",
            referencedColumnName = "id"
    )
    private Publisher publisher;

    @ManyToMany(mappedBy = "books")
    private List<Author> authors;

    @Enumerated
    private Genre genre;

    public Book() {
        super();
        this.authors = new ArrayList<>();
        this.libraries = new ArrayList<>();
    }

    public Book(String name) {
        super();
        this.name = name;
        this.published = new Date();
        this.authors = new ArrayList<>();
        this.libraries = new ArrayList<>();
    }

    public Date getPublished() {
        return (Date) published.clone();
    }

    public void setPublished(Date published) {
        this.published = (Date) published.clone();
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ISBN='" + ISBN + '\'' +
                ", published='" + published + '\'' +
                '}';
    }
}
