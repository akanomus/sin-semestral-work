package cvut.fel.sin.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "address")
@Getter @Setter
@NoArgsConstructor
public class Address extends AbstractEntity {

    @NotNull
    private String city;

    @NotNull
    private String street;

    @NotNull
    private String postalCode;

    @OneToOne(mappedBy = "address")
    private Library library;

    @OneToMany(mappedBy = "address")
    private List<Publisher> publisher;

    @OneToOne(mappedBy = "address")
    private Author author;

    public Address(String city, String street, String postalCode) {
        this();
        this.city = city;
        this.street = street;
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", postal code='" + postalCode + '\'' +
                '}';
    }
}
