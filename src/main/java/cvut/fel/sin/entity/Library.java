package cvut.fel.sin.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "library")
@Getter @Setter
public class Library extends AbstractEntity {

    @OneToOne
    @JoinColumn(
            name = "address_id",
            referencedColumnName = "id"
    )
    private Address address;

    @ManyToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JoinTable(
            name = "book_library",
            joinColumns = @JoinColumn(name = "book_id",
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "library_id",
                    referencedColumnName = "id")
    )
    private List<Book> books;

    public Library(String name) {
        this();
        this.name = name;
        this.books = new ArrayList<>();
    }

    public Library() {
        super();
        this.books = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Library{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

}
