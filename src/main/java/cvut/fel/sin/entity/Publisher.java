package cvut.fel.sin.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter @Setter
@Table(name = "publisher")
public class Publisher extends AbstractEntity {

    @ManyToOne
    @JoinColumn(
            name = "address_id",
            referencedColumnName = "id"
    )
    private Address address;

    @OneToMany(mappedBy = "publisher")
    private List<Book> books;

    @ManyToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JoinTable(
            name = "author_publisher",
            joinColumns = @JoinColumn(name = "author_id",
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "publisher_id",
                    referencedColumnName = "id")
    )
    private List<Author> authors;

    public Publisher(String name) {
        this();
        this.name = name;
    }

    public Publisher() {
        super();
        this.books = new ArrayList<>();
        this.authors = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Publisher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
