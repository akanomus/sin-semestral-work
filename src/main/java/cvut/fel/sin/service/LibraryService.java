package cvut.fel.sin.service;

import cvut.fel.sin.entity.Library;

public interface LibraryService {

    Library findById(Long id);

    Boolean addBookToLibrary(Long libraryId, Long bookId);

}
