package cvut.fel.sin.service;

import cvut.fel.sin.entity.Book;
import cvut.fel.sin.entity.Library;
import cvut.fel.sin.exception.FieldInvalidException;
import cvut.fel.sin.exception.FieldMissingException;
import cvut.fel.sin.exception.NotFoundException;
import cvut.fel.sin.repository.BookRepository;
import cvut.fel.sin.repository.LibraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class LibraryServiceImpl implements LibraryService{

    private final LibraryRepository libraryRepository;
    private final BookRepository bookRepository;

    @Autowired
    public LibraryServiceImpl(LibraryRepository libraryRepository, BookRepository bookRepository) {
        this.libraryRepository = libraryRepository;
        this.bookRepository = bookRepository;
    }

    @Transactional
    public Library findById(Long id) {
        if (id == null) {
            throw new FieldMissingException();
        }
        return libraryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("LIBRARY_NOT_FOUND"));
    }

    @Transactional
    public Boolean addBookToLibrary(Long libraryId, Long bookId) {
        if (libraryId == null || bookId == null) {
            throw new FieldMissingException();
        }

        Library library = libraryRepository.findById(libraryId)
                .orElseThrow(() -> new FieldInvalidException("LIBRARY_NOT_FOUND"));

        Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new FieldInvalidException("BOOK_NOT_FOUND"));

        assignBookToLibrary(library, book);

        libraryRepository.save(library);

        return true;
    }

    private void assignBookToLibrary(Library library, Book book) {
        List<Book> bookList = library.getBooks();
        bookList.add(book);
        library.setBooks(bookList);
    }
}
