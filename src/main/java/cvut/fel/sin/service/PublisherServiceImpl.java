package cvut.fel.sin.service;

import cvut.fel.sin.dto.request.BookCreate;
import cvut.fel.sin.entity.*;
import cvut.fel.sin.exception.FieldInvalidException;
import cvut.fel.sin.exception.FieldMissingException;
import cvut.fel.sin.exception.NotFoundException;
import cvut.fel.sin.repository.AuthorRepository;
import cvut.fel.sin.repository.BookRepository;
import cvut.fel.sin.repository.PublisherRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PublisherServiceImpl implements PublisherService {
    private static final Logger logger = LoggerFactory.getLogger(PublisherServiceImpl.class);

    private final PublisherRepository publisherRepository;
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    @Autowired
    public PublisherServiceImpl(
            PublisherRepository publisherRepository,
            AuthorRepository authorRepository,
            BookRepository bookRepository
    ) {
        this.publisherRepository = publisherRepository;
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    @Transactional
    public Publisher findById(Long id) {
        if (id == null) {
            throw new FieldMissingException();
        }
        return publisherRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("PUBLISHER_NOT_FOUND"));
    }

    @Transactional
    public Boolean createContract(Long authorId, Long publisherId) {
        if (authorId == null || publisherId == null) {
            throw new FieldMissingException();
        }

        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new NotFoundException("AUTHOR_NOT_FOUND"));
        Publisher publisher = publisherRepository.findById(publisherId)
                .orElseThrow(() -> new NotFoundException("PUBLISHER_NOT_FOUND"));

        if (validateAuthorNotAlreadyAssigned(author, publisher)) {
            throw new FieldInvalidException("AUTHOR_ALREADY_ASSIGNED");
        }

        assignAuthorToPublisher(author, publisher);

        authorRepository.save(author);
        publisherRepository.save(publisher);

        return true;
    }

    private Boolean validateAuthorNotAlreadyAssigned(Author author, Publisher publisher) {
        return publisher.getAuthors().contains(author);
    }

    private void assignAuthorToPublisher(Author author, Publisher publisher) {
        publisher.getAuthors().add(author);
    }

    private void assignPublisherToAuthor(Publisher publisher, Author author) {
        author.getPublishers().add(publisher);
    }

    @Transactional
    public Long publishNewBook(Long publisherId, BookCreate bookCreate) {
//        if (publisherId == null || bookCreate == null) {
//            logger.error("Fields are missing!");
//            throw new FieldMissingException();
//        }
//
//        Publisher publisher = publisherRepository.findById(publisherId)
//                .orElseThrow(() -> new NotFoundException("PUBLISHER_NOT_FOUND"));
//        logger.info("Found publisher: {}", publisher);
//
//        if (validateBookNotAlreadyPublished(bookCreate, publisher)) {
//            logger.warn("Didn't pass validation!");
//            logger.error("Exception of type: {}", FieldInvalidException.class);
//            throw new FieldInvalidException("BOOK_ALREADY_PUBLISHED");
//        }
//
//        Book book = createToBook(bookCreate, publisher);
//        logger.info("Created book from bookCreate: {}", book);
//
//        assignBookToPublisher(publisher, book);
//        logger.info("Book is assigned to publisher");
//
//        assignPublisherToBook(book, publisher);
//        logger.info("Publisher is assigned to book");
//
//        bookRepository.save(book);
//        logger.info("Book is saved");
//        publisherRepository.save(publisher);
//        logger.info("Publisher is saved");
//
//        return book.getId();

        Publisher publisher = findPublisherById(publisherId);
        logger.trace("Publisher found: {}", publisher);

        Book book = bookCreateToBook(bookCreate);

        logger.trace("CreateBook transformed to book: {}", book);

        if (!validateBookNotAlreadyPublished(bookCreate, publisher)) {
            logger.warn("Validation is not passed");
            logger.error("Exception of type: {}", FieldInvalidException.class);
            throw new FieldInvalidException("BOOK_IS_ALREADY_PUBLISHED");
        } else {
            assignPublisherToBook(book, publisher);
            logger.info("Publisher is assigned to book");
            assignBookToPublisher(publisher,book);
            logger.info("Book is assigned to publisher");

            bookRepository.save(book);
            logger.info("Book is saved");
            publisherRepository.save(publisher);
            logger.info("Publisher is saved");
        }
        return book.getId();
    }

    private Boolean validateBookNotAlreadyPublished(BookCreate bookCreate, Publisher publisher) {
        return publisher.getBooks()
                .stream()
                .filter(b -> Objects.equals(b.getISBN(), bookCreate.getIsbn()))
                .collect(Collectors.toList())
                .isEmpty();
    }

    private void assignBookToPublisher(Publisher publisher, Book book) {
        publisher.getBooks().add(book);
    }

    private void assignPublisherToBook(Book book, Publisher publisher) {
        book.setPublisher(publisher);
    }

    private Book bookCreateToBook(BookCreate bookCreate) {
        Book book = new Book(bookCreate.getName());
        book.setISBN(bookCreate.getIsbn());
        book.setGenre(
                Genre.values()[Math.toIntExact(bookCreate.getGenre())]
        );
        book.setAuthors(
                bookCreate.getAuthors()
                .stream()
                .map(this::findAuthorById)
                .collect(Collectors.toList())
        );
        return book;
    }

    private Author findAuthorById(Long id) {
        if(id == null){
            throw new FieldMissingException();
        }

        Optional<Author> authorOptional = authorRepository.findById(id);

        if(!authorOptional.isPresent()){
            throw new NotFoundException("Author was not found");
        }
        return authorOptional.get();
    }

    public Publisher findPublisherById(Long id) {
        if (id == null)
            throw new FieldMissingException();

        Optional<Publisher> publisherOptional = publisherRepository.findById(id);

        if (!publisherOptional.isPresent()) {
            throw new NotFoundException("PUBLISHER_NOT_FOUND");
        }

        return publisherOptional.get();
    }
}
