package cvut.fel.sin.service;

import cvut.fel.sin.entity.Author;
import cvut.fel.sin.exception.FieldMissingException;
import cvut.fel.sin.exception.NotFoundException;
import cvut.fel.sin.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Transactional
    public Author findById(Long id) {
        if (id == null) {
            throw new FieldMissingException();
        }
        return authorRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("AUTHOR_NOT_FOUND"));
    }
}
