package cvut.fel.sin.service;


import cvut.fel.sin.dto.request.BookCreate;
import cvut.fel.sin.entity.Book;
import cvut.fel.sin.entity.Publisher;

public interface PublisherService {

    Publisher findById(Long id);

    Boolean createContract(Long authorId, Long publisherId);

    Long publishNewBook(Long publisherId, BookCreate bookCreate);

}
