package cvut.fel.sin.service;

import cvut.fel.sin.entity.Address;

public interface AddressService {

    Address findById(Long id);

}
