package cvut.fel.sin.service;

import cvut.fel.sin.entity.Author;

public interface AuthorService {

    Author findById(Long id);

}
