package cvut.fel.sin.service;

import cvut.fel.sin.entity.Address;
import cvut.fel.sin.exception.FieldMissingException;
import cvut.fel.sin.exception.NotFoundException;
import cvut.fel.sin.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AddressServiceImpl implements AddressService{

    private final AddressRepository addressRepository;

    @Autowired
    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Transactional
    public Address findById(Long id) {
        if (id == null) {
            throw new FieldMissingException();
        }
        return addressRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("ADDRESS_NOT_FOUND"));
    }
}
