package cvut.fel.sin.service;

import cvut.fel.sin.entity.Book;
import cvut.fel.sin.exception.FieldMissingException;
import cvut.fel.sin.exception.NotFoundException;
import cvut.fel.sin.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class BookServiceImpl implements BookService{

    private final BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Transactional
    public Book findById(Long id) {
        if (id == null) {
            throw new FieldMissingException();
        }
        return bookRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("BOOK_NOT_FOUND"));
    }

}
