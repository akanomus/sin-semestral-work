package cvut.fel.sin.service;

import cvut.fel.sin.entity.Book;

public interface BookService {

    Book findById(Long id);

}
