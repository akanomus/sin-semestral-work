package cvut.fel.sin;

import cvut.fel.sin.entity.*;
import cvut.fel.sin.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SinSemestralWorkApplication implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(SinSemestralWorkApplication.class);

	@Autowired
	private BookRepository bookRepository;
	@Autowired
	private LibraryRepository libraryRepository;
	@Autowired
	private AddressRepository addressRepository;
	@Autowired
	private PublisherRepository publisherRepository;
	@Autowired
	private AuthorRepository authorRepository;

	public static void main(String[] args) {
		SpringApplication.run(SinSemestralWorkApplication.class, args);
	}

	@Override
	public void run(String... args) {

//		log.info("StartApplication...");
//
//		Publisher publisher = new Publisher("Pub");
//
//		Book book1 = new Book("Java");
//		book1.setISBN("1");
//		Book book2 = new Book("Node");
//		book2.setISBN("2");
//		Book book3 = new Book("Python");
//		book3.setISBN("3");
//		bookRepository.save(book1);
//		bookRepository.save(book2);
//		bookRepository.save(book3);
//
//		System.out.println("\nfindAll()");
//		bookRepository.findAll().forEach(System.out::println);
//
//		System.out.println("\nfindById(1L)");
//		bookRepository.findById(1L).ifPresent(System.out::println);
//
//		System.out.println("\nfindByName('Node')");
//		bookRepository.findByName("Java").forEach(System.out::println);

		log.info("StartApplication...");

		// Books
		Book book1 = new Book("1");
		book1.setName("Java");
		Book book2 = new Book("2");
		book2.setName("Node");
		Book book3 = new Book("3");
		book3.setName("Python");
		bookRepository.save(book1);
		bookRepository.save(book2);
		bookRepository.save(book3);

		// Address of library
		Address address = new Address();
		address.setCity("Prague");
		address.setStreet("Technicka 17");
		address.setPostalCode("16000");
		address.setStreet("Parizska");
		addressRepository.save(address);

		// Library
		Library library = new Library();
		library.setName("My new library");
		library.setAddress(address);
		System.out.println(library.getId());
		libraryRepository.save(library);

		// Publisher
		Publisher publisher = new Publisher();
		publisher.setAddress(address);
		publisherRepository.save(publisher);

		// Author
		Author author = new Author();
		author.setName("Fedor");
		author.setSurname("Dostoyevski");
		author.setAddress(address);
		author.setEmail("theUndergroundMan@gmail.com");
		authorRepository.save(author);

		// Prints
		System.out.println("\nlibraries findAll()");
		libraryRepository.findAll().forEach(System.out::println);

		System.out.println("\nfindAll()");
		bookRepository.findAll().forEach(System.out::println);

		System.out.println("\nfindById(1L)");
		bookRepository.findById(1L).ifPresent(System.out::println);

		System.out.println("\nfindByName('Node')");
		bookRepository.findByName("Node").forEach(System.out::println);

		System.out.println("\nfindById(1L)");
		publisherRepository.findById(1L).ifPresent(System.out::println);


	}

}
