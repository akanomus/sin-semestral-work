package cvut.fel.sin.controller;

import cvut.fel.sin.dto.DTOMapper;
import cvut.fel.sin.dto.PublisherDTO;
import cvut.fel.sin.dto.request.BookCreate;
import cvut.fel.sin.service.BookService;
import cvut.fel.sin.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PublisherController {

    private final PublisherService publisherService;
    private final DTOMapper dtoMapper;

    @Autowired
    public PublisherController(PublisherService publisherService, BookService bookService, DTOMapper dtoMapper) {
        this.publisherService = publisherService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/publisher/{id}")
    public ResponseEntity<PublisherDTO> getPublisherById(@PathVariable Long id) {
        return ResponseEntity.ok(dtoMapper.publisherToDto(publisherService.findById(id)));
    }

    @PostMapping("/publishNewBook/{publisherId}")
    public ResponseEntity<Long> publishNewBook(
            @PathVariable("publisherId") Long publisherId,
            @RequestBody BookCreate bookCreate
    ) {
        return ResponseEntity.ok(publisherService.publishNewBook(publisherId, bookCreate));
    }

    @PostMapping("/createContract/{authorId}/{publisherId}")
    public ResponseEntity<Boolean> createContract(
            @PathVariable("authorId") Long authorId,
            @PathVariable("publisherId") Long publisherId
    ) {
        return ResponseEntity.ok(publisherService.createContract(authorId, publisherId));
    }
}
