package cvut.fel.sin.controller;

import cvut.fel.sin.dto.DTOMapper;
import cvut.fel.sin.dto.LibraryDTO;
import cvut.fel.sin.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LibraryController {

    private final LibraryService libraryService;
    private final DTOMapper dtoMapper;

    @Autowired
    public LibraryController(LibraryService libraryService, DTOMapper dtoMapper) {
        this.libraryService = libraryService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/library/{id}")
    public ResponseEntity<LibraryDTO> getLibraryById(@PathVariable Long id) {
        return ResponseEntity.ok(dtoMapper.libraryToDto(libraryService.findById(id)));
    }

    @PostMapping("/addBookToLibrary/{bookId}/{libraryId}")
    public ResponseEntity<Boolean> addBookToLibrary(
            @PathVariable("bookId") Long bookId,
            @PathVariable("libraryId") Long libraryId
    ) {
        return ResponseEntity.ok(libraryService.addBookToLibrary(libraryId, bookId));
    }
}
