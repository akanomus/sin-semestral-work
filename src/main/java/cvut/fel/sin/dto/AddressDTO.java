package cvut.fel.sin.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressDTO extends AbstractDTO{

    private String city;
    private String street;
    private String postalCode;

    public AddressDTO() {
    }

    public AddressDTO(AddressDTO dto) {
        super(dto);
        this.city = dto.getCity();
        this.street = dto.getStreet();
        this.postalCode = dto.getPostalCode();
    }

    @Override
    protected AbstractDTO clone() {
        return new AddressDTO(this);
    }
}
