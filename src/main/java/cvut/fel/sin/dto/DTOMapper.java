package cvut.fel.sin.dto;

import cvut.fel.sin.entity.*;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DTOMapper {


    AddressDTO addressToDto(Address address);

    AuthorDTO authorToDto(Author author);

    BookDTO bookToDto(Book book);

    LibraryDTO libraryToDto(Library library);

    PublisherDTO publisherToDto(Publisher publisher);

}
