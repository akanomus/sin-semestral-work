package cvut.fel.sin.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PublisherDTO extends AbstractDTO {

    public PublisherDTO() {
    }

    public PublisherDTO(PublisherDTO dto) {
        super(dto);
    }

    @Override
    protected AbstractDTO clone() {
        return new PublisherDTO(this);
    }
}
