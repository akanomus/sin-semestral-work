package cvut.fel.sin.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthorDTO extends AbstractDTO {

    private String surname;
    private String email;

    public AuthorDTO() {
    }

    public AuthorDTO(AuthorDTO dto) {
        super(dto);
        this.surname = dto.getSurname();
        this.email = dto.getEmail();
    }

    @Override
    protected AbstractDTO clone() {
        return new AuthorDTO(this);
    }
}
