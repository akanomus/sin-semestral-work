package cvut.fel.sin.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LibraryDTO extends AbstractDTO{

    public LibraryDTO() {
    }

    public LibraryDTO(LibraryDTO dto) {
        super(dto);
    }

    @Override
    protected AbstractDTO clone() {
        return new LibraryDTO(this);
    }
}
