package cvut.fel.sin.repository;

import cvut.fel.sin.entity.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long> {
}
