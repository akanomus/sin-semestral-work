package cvut.fel.sin.repository;

import cvut.fel.sin.entity.Address;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Long> {
}
