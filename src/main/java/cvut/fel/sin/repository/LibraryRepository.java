package cvut.fel.sin.repository;

import cvut.fel.sin.entity.Library;
import org.springframework.data.repository.CrudRepository;

public interface LibraryRepository extends CrudRepository<Library, Long> {
}
